var express           = require('express');
var app               = express();
var http              = require('http').Server(app);
var bodyParser        = require('body-parser');
var cookieParser      = require('cookie-parser');
var exprhbs           = require('express-handlebars');
var expressValidator  = require('express-validator');
var flash             = require('connect-flash');
var io                = require('socket.io')(http);
var mongoose          = require('mongoose');
var passport          = require('passport');
var path              = require('path');
var session           = require('express-session');
var LocalStrategy     = require('passport-local').Strategy;

var routes            = require('./routes/index');
var users             = require('./routes/users');
var messages          = require('./routes/messages');

// Init database
mongoose.Promise = Promise;
const dbUrl = 'mongodb://aleph:fPe-MpQ-UpG-u7w@ds131800.mlab.com:31800/ibs';
mongoose.connect(dbUrl);
var db = mongoose.connection;

// Set up middleware
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', exprhbs({defaultLayout: 'layout'}));
app.set('view engine', 'handlebars');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

// Set up static folder
app.use(express.static(path.join(__dirname, 'public')));

// Set up express-session
app.use(session({
  secret: 'secret',
  saveUninitialized: true,
  resave: true
}));

// Set up passport
app.use(passport.initialize());
app.use(passport.session());

// Set up express-validator
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
    var namespace = param.split('.')
    , root        = namespace.shift()
    , formParam   = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));

// Set up connect-flash
app.use(flash());

// Global variables
app.use(function(req, res, next) {
  res.locals.success_msg  = req.flash('success_msg');
  res.locals.error_msg    = req.flash('error_msg');
  res.locals.error        = req.flash('error');
  res.locals.user         = req.user || null;
  next();
});

app.use('/', routes);
app.use('/users', users);
app.use('/messages', messages);

app.set('port', 8080);

var server = http.listen(app.get('port'), function(){
	console.log('Server started on port ' + app.get('port'));
});

// Socket functions
module.exports.emitMessage = function(message) {
  console.log('emitMessage called');
  io.emit('message', message);
}
