var mongoose    = require('mongoose');

var MessageSchema = mongoose.Schema({
  username: String,
  content: String,
  lat: Number,
  lon: Number,
  date: String
});

var Message = module.exports = mongoose.model('Message', MessageSchema);

module.exports.createMessage = function(newMessage, callback) {
  newMessage.save(callback);
}
