var express       = require('express');
var router        = express.Router();
var main          = require('../app.js');
var Message       = require('../models/message');

router.get('/', function(req, res) {
  Message.find().lean().exec(function(err, messages) {
    if (err) throw err;
    res.json(messages);
  });
});

// TODO wrap DB call in try-catch
router.post('/', function(req, res) {

  var raw_message = {
    username: req.user.username,
    content: req.body.content,
    lat: req.body.lat,
    lon: req.body.lon,
    date: new Date().toJSON()
  }
  // Save message to database
  var message = new Message(raw_message);
  var savedMessage = message.save();

  main.emitMessage(raw_message);

  res.sendStatus(200);
});

module.exports = router;
